# ColorCat
ColorCat 是开源免费的调色取色工具，提供了屏幕取色、调色板调色、配色方案、颜色转换功能。</br>
![输入图片说明](https://git.oschina.net/uploads/images/2017/0828/164125_48798577_2349.png "colorcat.png")

以前开发的小工具[colorcat](http://https://code.google.com/archive/p/colorcat/)，现在转到码云上。<br/>
更新：<br/>
1、项目更改为maven管理。<br/>
2、jna包更新到4.4.0<br/>