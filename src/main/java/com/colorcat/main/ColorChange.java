package com.colorcat.main;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 屏幕取色工具
 * 
 * @author penngo(https://my.oschina.net/penngo)
 * @version 2017.08.28
 */
public class ColorChange extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField htmlField;
	private JSpinner redSpinner;
	private JSpinner greenSpinner;
	private JSpinner blueSpinner;
	private JLabel cruColor;
	/**
	 * Create the panel.
	 */
	public ColorChange() {
		setLayout(null);
		
		JButton btnHtmlrgb = new JButton("HTML转RGB→");
		btnHtmlrgb.setBounds(147, 74, 110, 30);
		btnHtmlrgb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					String hex = htmlField.getText().trim();
					if(hex.length() == 6){
//						hex = hex.replaceFirst("#", "");
						Color color = new Color(Integer.valueOf(hex, 16));
						cruColor.setBackground(color);
						redSpinner.setValue(color.getRed());
						greenSpinner.setValue(color.getGreen());
						blueSpinner.setValue(color.getBlue());
					}
					else{
						//JOptionPane.showMessageDialog(parentComponent, message);
						JOptionPane.showMessageDialog(ColorChange.this, "HTML转RGB出错，请检查HTML值！");
					}
				}
				catch(Exception ex){
					//ex.printStackTrace();
					JOptionPane.showMessageDialog(ColorChange.this, "HTML转RGB出错，请检查HTML值！");
				}
			}
		});
		add(btnHtmlrgb);
		
		JButton btnRgbhtml = new JButton("←RGB转HTML");
		btnRgbhtml.setBounds(147, 114, 110, 30);
		btnRgbhtml.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					int red = (Integer)redSpinner.getValue();
					int green = (Integer)greenSpinner.getValue();
					int blue = (Integer)blueSpinner.getValue();
					Color color = new Color(red, green, blue);
					cruColor.setBackground(color);
					String hex = Integer.toHexString(color.getRGB());
					System.out.println(hex);
					htmlField.setText(hex.replaceFirst("ff", ""));
				}
				catch(Exception ex){
					ex.printStackTrace();
					JOptionPane.showMessageDialog(ColorChange.this, "RGB转HTML出错，请检查RGB值！");
				}
			}
		});
		add(btnRgbhtml);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(15, 18, 130, 200);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblHtml = new JLabel("HTML:#");
		lblHtml.setBounds(15, 86, 40, 18);
		panel.add(lblHtml);
		
		htmlField = new JTextField();
		htmlField.setText("000000");
		htmlField.setFont(new Font("SansSerif", Font.PLAIN, 16));
		htmlField.setBounds(52, 85, 70, 20);
		panel.add(htmlField);
		htmlField.setColumns(10);
		
		JLabel lblfff = new JLabel("(如:#f0f0f0)");
		lblfff.setBounds(51, 110, 73, 18);
		panel.add(lblfff);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(260, 18, 130, 200);
		add(panel_1);
		panel_1.setLayout(null);
		
		redSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1) );
		redSpinner.setBounds(61, 35, 60, 20);
		panel_1.add(redSpinner);
		
		JLabel label = new JLabel("红:");
		label.setBounds(45, 30, 24, 30);
		panel_1.add(label);
		
		greenSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1) );
		greenSpinner.setBounds(61, 85, 60, 20);
		panel_1.add(greenSpinner);
		
		JLabel label_1 = new JLabel("绿:");
		label_1.setBounds(45, 80, 24, 30);
		panel_1.add(label_1);
		
		blueSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 255, 1) );
		blueSpinner.setBounds(61, 134, 60, 20);
		panel_1.add(blueSpinner);
		
		JLabel label_2 = new JLabel("蓝:");
		label_2.setBounds(45, 130, 24, 24);
		panel_1.add(label_2);
		
		cruColor = new JLabel("");
		cruColor.setBounds(157, 29, 91, 33);
		cruColor.setOpaque(true);
		cruColor.setBorder(BorderFactory.createLineBorder(new Color(128, 128,
				128), 3));
		add(cruColor);

	}
}
