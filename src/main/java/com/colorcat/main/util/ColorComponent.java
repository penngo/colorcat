package com.colorcat.main.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 屏幕取色工具
 * 
 * @author penngo(https://my.oschina.net/penngo/blog)
 * @version 2017.08.28
 */
public class ColorComponent extends JPanel{
	private static final long serialVersionUID = 1L;
	private JLabel colorLabel;
	public ColorComponent(Object label, Object color, int width){
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createLineBorder(this.getBackground()));
		colorLabel = new JLabel("       ");
		colorLabel.setPreferredSize(new Dimension(45, 20));
		
		colorLabel.setOpaque(true);
		colorLabel.setBackground(new Color((Integer)color));
		
		JLabel name = new JLabel(String.valueOf(label));
		name.setPreferredSize(new Dimension(width, 20));
		name.setOpaque(true);
		
		this.add(colorLabel, new GB(0, 0, 1, 1).setInsets(2, 2, 2, 0));
		this.add(name, new GB(1, 0, 1, 1).setFill(GB.BOTH).setWeight(100, 0).setInsets(0, 5, 0, 10));
		this.setOpaque(true);
	}
	
	public ColorComponent(Object color, int width){
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createLineBorder(this.getBackground()));
		colorLabel = new JLabel(" ");
		colorLabel.setPreferredSize(new Dimension(20, 20));
		colorLabel.setBorder(BorderFactory.createBevelBorder(2));
		colorLabel.setOpaque(true);
		colorLabel.setBackground(new Color((Integer)color));

		this.add(colorLabel, new GB(0, 0, 1, 1).setInsets(1, 1, 1, 1).setFill(GB.BOTH).setWeight(100, 0));
//		this.add(name, new GB(1, 0, 1, 1).setFill(GB.BOTH).setWeight(100, 0).setInsets(0, 5, 0, 10));
		this.setOpaque(true);
	}
	
	public Color getColor(){
		return colorLabel.getBackground();
	}
}
