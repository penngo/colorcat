package com.colorcat.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.colorcat.main.util.ColorComponent;
import com.colorcat.main.util.GB;

/**
 * 屏幕取色工具
 * 
 * @author penngo(https://my.oschina.net/penngo)
 * @version 2017.08.28
 */
public class NameColorPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabPanel;
	private JLabel colorPan;
	private JLabel textPan;
	
	private JTextField htmlText;
	private JTextField redText;
	private JTextField greenText;
	private JTextField blueText;
	
	private Object[][] awtColor = new Object[][]{
			{"WHITE", 0xffffff},
			{"LIGHT_GRAy", 0xc0c0c0},
			{"GRAY", 0x808080},
			{"DARK_GRAY", 0x404040},
			{"BLACK", 0x000000},
			{"RED",	0xff0000},
			{"PINK", 0xffafaf},
			{"ORANGE", 0xffc800},
			{"YELLOW", 0xffff00},
			{"GREEN", 0x00ff00},
			{"MAGENTA", 0xff00ff},
			{"CYAN", 0x00ffff},
			{"BLUE", 0x0000ff}
	};
	
	private Object[][] systemColor = new Object[][]{
			{"desktop", 0x000000},
			{"activeCaption", 0x99b4d1},
			{"activeCaption Text", 0x000000},
			{"activeCaptionBorder", 0xb4b4b4},
			{"inactiveCaption", 0xbfcddb},
			{"inactiveCaptionText", 0x434e54},
			{"inactiveCaptionBorder", 0xf4f7fc},
			{"window", 0xffffff},
			{"windowBorder", 0x646464},
			{"windowText", 0x000000},
			{"menu", 0xf0f0f0},
			{"menuText", 0x000000},
			{"text", 0xffffff},
			{"textText", 0x000000},
			{"textHighlight", 0x3399ff},
			{"textHighlightText", 0xffffff},
			{"textInactiveText", 0x6d6d6d},
			{"control", 0xf0f0f0},
			{"controlText", 0x000000},
			{"controlHighlight", 0xe3e3e3},
			{"controlLtHighlight", 0xffffff},
			{"controlShadow", 0xa0a0a0},
			{"controlDkShadow", 0x696969},
			{"scrollbar", 0xc8c8c8},
			{"info", 0xffffe1},
			{"infoText", 0x000000}
	};
	
	private Object[] nameColor = new Object[]{
			0x000000,
			0x696969,
			0x696969,
			0x808080,
			0x808080,
			0xa9a9a9,
			0xa9a9a9,
			0xc0c0c0,
			0xd3d3d3,
			0xd3d3d3,
			0xdcdcdc,
			0xf5f5f5,
			0xffffff,
			0xd8bfd8,
			0xbc8f8f,
			0xdda0dd,
			0xcd5c5c,
			0xa42a2a,
			0xb22222,
			0xee82ee,
			0xf08080,
			0x800000,
			0x800080,
			0x8b0000,
			0x8b008b,
			0xff0000,
			0xff00ff,
			0xff00ff,
			0xfffafa,
			0xda70d6,
			0xffe4e1,
			0xfa8072,
			0xff6347,
			0xe9967a,
			0xff7f50,
			0xff4500,
			0xffa07a,
			0xa0522d,
			0xc71585,
			0xfff5ee,
			0xd2691e,
			0x8b4513,
			0xf4a460,
			0xff1493,
			0xffdab9,
			0xcd853f,
			0xff69b4,
			0xfaf0e6,
			0xffe4c4,
			0xff8c00,
			0xdeb887,
			0xd2b48c,
			0xfaebd7,
			0xffdead,
			0xffebcd,
			0xffefd5,
			0xffe4b5,
			0xffa500,
			0xf5deb3,
			0xfdf5e6,
			0xfff0f5,
			0xfffaf0,
			0xdb7093,
			0xb8860b,
			0xdaa520,
			0xdc143c,
			0xfff8dc,
			0xffc0cb,
			0xffd700,
			0xffb6c1,
			0xf0e68c,
			0xfffacd,
			0xeee8aa,
			0xbdb76b,
			0xf5f5dc,
			0xfafad2,
			0x808000,
			0xffff00,
			0xffffe0,
			0xfffff0,
			0x6b8e23,
			0x9acd32,
			0x556b2f,
			0xadff2f,
			0x7fff00,
			0x7cfc00,
			0x8fbc8f,
			0x228b22,
			0x32cd32,
			0x90ee90,
			0x98fb98,
			0x006400,
			0x008000,
			0x00ff00,
			0xf0fff0,
			0x2e8b57,
			0x3cb371,
			0x00ff7f,
			0xf5fffa,
			0x00fa9a,
			0x66cdaa,
			0x7fffd4,
			0x40e0d0,
			0x20b2aa,
			0x48d1cc,
			0x2f4f4f,
			0x2f4f4f,
			0xafeeee,
			0x008080,
			0x008b8b,
			0x00ffff,
			0x00ffff,
			0xe0ffff,
			0xf0ffff,
			0x00ced1,
			0x5f9ea0,
			0xb0e0e6,
			0xadd8e6,
			0x00bfff,
			0x87ceeb,
			0x87cefa,
			0x4682b4,
			0xf0f8ff,
			0x1e90ff,
			0x708090,
			0x708090,
			0x778899,
			0x778899,
			0xb0c4de,
			0x6495ed,
			0x4169e1,
			0x191970,
			0xe6e6fa,
			0x000080,
			0x00008b,
			0x0000cd,
			0x0000ff,
			0xf8f8ff,
			0x6a5acd,
			0x483d8b,
			0x7b68ee,
			0x9370db,
			0x8a2be2,
			0x4b0082,
			0x9932cc,
			0x9400d3,
			0xba55d3
	};
	
	private Object[] webSaveColor = new Object[]{
			0x000000,
			0x330000,
			0x330033,
			0x333300,
			0x003300,
			0x003333,
			0x000033,
			0x333333,
			0x660000,
			0x660066,
			0x660033,
			0x663300,
			0x666600,
			0x336600,
			0x006600,
			0x006633,
			0x006666,
			0x003366,
			0x000066,
			0x330066,
			0x663333,
			0x663366,
			0x990000,
			0x990099,
			0x993300,
			0x990066,
			0x996600,
			0x990033,
			0x666633,
			0x999900,
			0x669900,
			0x339900,
			0x336633,
			0x009900,
			0x009933,
			0x009966,
			0x336666,
			0x009999,
			0x006699,
			0x003399,
			0x333366,
			0x000099,
			0x330099,
			0x660099,
			0x666666,
			0x993333,
			0x993399,
			0xcc0000,
			0xcc00cc,
			0xcc0099,
			0xcc3300,
			0x996633,
			0x993366,
			0xcc0066,
			0xcc6600,
			0xcc0033,
			0xcc9900,
			0x999933,
			0xcccc00,
			0x99cc00,
			0x669933,
			0x66cc00,
			0x33cc00,
			0x339933,
			0x00cc00,
			0x00cc33,
			0x339966,
			0x00cc66,
			0x00cc99,
			0x339999,
			0x00cccc,
			0x0099cc,
			0x336699,
			0x0066cc,
			0x0033cc,
			0x333399,
			0x0000cc,
			0x3300cc,
			0x663399,
			0x6600cc,
			0x9900cc,
			0x996666,
			0x996699,
			0xcc3333,
			0xcc33cc,
			0xff0000,
			0xff00ff,
			0xff00cc,
			0xff3300,
			0xcc3399,
			0xcc6633,
			0xff0099,
			0xff6600,
			0xff0066,
			0xff9900,
			0xcc3366,
			0xcc9933,
			0xff0033,
			0xffcc00,
			0x999966,
			0xcccc33,
			0xffff00,
			0xccff00,
			0x99cc33,
			0x99ff00,
			0x66ff00,
			0x66cc33,
			0x33ff00,
			0x669966,
			0x33cc33,
			0x00ff00,
			0x00ff33,
			0x33cc66,
			0x00ff66,
			0x00ff99,
			0x33cc99,
			0x00ffcc,
			0x669999,
			0x33cccc,
			0x00ffff,
			0x00ccff,
			0x3399cc,
			0x0099ff,
			0x0066ff,
			0x3366cc,
			0x0033ff,
			0x666699,
			0x3333cc,
			0x0000ff,
			0x3300ff,
			0x6633cc,
			0x6600ff,
			0x9900ff,
			0x9933cc,
			0xcc00ff,
			0x999999,
			0xcc6666,
			0xcc66cc,
			0xff3333,
			0xff33ff,
			0xff33cc,
			0xff6633,
			0xcc6699,
			0xff3399,
			0xcc9966,
			0xff9933,
			0xff3366,
			0xffcc33,
			0xcccc66,
			0xffff33,
			0xccff33,
			0x99cc66,
			0x99ff33,
			0x66ff33,
			0x66cc66,
			0x33ff33,
			0x33ff66,
			0x66cc99,
			0x33ff99,
			0x33ffcc,
			0x66cccc,
			0x33ffff,
			0x33ccff,
			0x6699cc,
			0x3399ff,
			0x3366ff,
			0x6666cc,
			0x3333ff,
			0x6633ff,
			0x9966cc,
			0x9933ff,
			0xcc33ff,
			0xff6666,
			0xff66ff,
			0xff9966,
			0xff66cc,
			0xff6699,
			0xffcc66,
			0xffff66,
			0xccff66,
			0x99ff66,
			0x66ff66,
			0x66ff99,
			0x66ffcc,
			0x66ffff,
			0x66ccff,
			0x6699ff,
			0x6666ff,
			0x9966ff,
			0xcc66ff,
			0xcc9999,
			0xcc99cc,
			0xcccc99,
			0x99cc99,
			0x99cccc,
			0x9999cc,
			0xcccccc,
			0xff9999,
			0xff99ff,
			0xff99cc,
			0xffcc99,
			0xffff99,
			0xccff99,
			0x99ff99,
			0x99ffcc,
			0x99ffff,
			0x99ccff,
			0x9999ff,
			0xcc99ff,
			0xffcccc,
			0xffccff,
			0xffffcc,
			0xccffcc,
			0xccffff,
			0xccccff,
			0xffffff
	};
	
	public NameColorPanel() {
		setLayout(new GridBagLayout());
		JPanel topPanel = new JPanel(new GridBagLayout());
		SelectMouseListener l = new SelectMouseListener();
		htmlText = new JTextField(7);
		htmlText.setEditable(false);
		htmlText.addMouseListener(l);
		
		redText = new JTextField(3);
		redText.setEditable(false);
		redText.addMouseListener(l);
		
		greenText = new JTextField(3);
		greenText.setEditable(false);
		greenText.addMouseListener(l);
		
		blueText = new JTextField(3);
		blueText.setEditable(false);
		blueText.addMouseListener(l);

		topPanel.add(new JLabel("  HTML:"), new GB(0, 0, 1, 1));
		topPanel.add(htmlText, new GB(1, 0, 1, 1));

		topPanel.add(new JLabel("           红:"), new GB(2, 0, 1, 1));
		topPanel.add(redText, new GB(3, 0, 1, 1));

		topPanel.add(new JLabel("  绿:"), new GB(4, 0, 1, 1));
		topPanel.add(greenText, new GB(5, 0, 1, 1));

		topPanel.add(new JLabel("  蓝:"), new GB(6, 0, 1, 1));
		topPanel.add(blueText, new GB(7, 0, 1, 1));
		topPanel.add(new JLabel(), new GB(8, 0, 1, 1).setFill(GB.BOTH)
				.setWeight(100, 0));

		this.add(topPanel, new GB(0, 0, 1, 1).setFill(GB.BOTH)
				.setWeight(100, 0));

		colorPan = new JLabel();
		colorPan.setOpaque(true);
		textPan = new JLabel("ColorCat", JLabel.CENTER);
		textPan.setFont(new Font("SansSerif", Font.BOLD, 24));
		Color co = textPan.getBackground();
		textPan.setForeground(new Color(co.getRed(), co.getGreen(), co.getBlue()));
		JPanel bannerPanel = new JPanel(new GridBagLayout());
		bannerPanel.add(colorPan, new GB(0, 0, 1, 1).setFill(GB.BOTH)
				.setWeight(100, 0));
		bannerPanel.add(textPan, new GB(1, 0, 1, 1));
		TitledBorder bannerBorder = new TitledBorder("");
		bannerBorder.setTitlePosition(TitledBorder.TOP);
		bannerPanel.setBorder(bannerBorder);
		this.add(bannerPanel, new GB(0, 1, 1, 1).setFill(GB.BOTH)
				.setWeight(100, 0) );
		
		tabPanel = new JTabbedPane();
		tabPanel.add("普通色", initColorPanel(awtColor, 75));
		tabPanel.add("系统色", initColorPanel(systemColor, 130));
		tabPanel.add("常用色", initColorPanel(nameColor, 130));
		tabPanel.add("网页安全色", initColorPanel(webSaveColor, 130));
		
		this.add(tabPanel, new GB(0, 2, 1, 1).setFill(GB.BOTH)
				.setWeight(100, 100) );
	}
	

	
	private JPanel initColorPanel(Object[][] colorArray, int width){
		JPanel mainPanel = new JPanel(new GridBagLayout());
		TitledBorder tabBorder = new TitledBorder("");
		mainPanel.setBorder(tabBorder);
		ColorMouseListener l = new ColorMouseListener();
		for(int row = 0; row < colorArray.length; row++){
			ColorComponent c = new ColorComponent(colorArray[row][0], colorArray[row][1], width);
			c.addMouseListener(l);
			if(row%2 == 0){
				mainPanel.add(c, new GB(0, row/2+1, 1, 1)
				.setInsets(0, 10, 0, 0));
			}
			else if(row%2 == 1){
				mainPanel.add(c, new GB(1, row/2+1, 1, 1)
				.setInsets(0, 5, 0, 0));
				mainPanel.add(new JLabel(), new GB(2, row/2+1, 1, 1).setFill(GB.BOTH)
						.setWeight(100, 0));
			}
		}
		mainPanel.add(new JPanel(), new GB(0, colorArray.length+1, 4, 1).setFill(GB.BOTH).setWeight(100, 100));

		return mainPanel;
	}
	private JPanel initColorPanel(Object[] colorArray, int width){
		JPanel mainPanel = new JPanel(new GridBagLayout());
		TitledBorder tabBorder = new TitledBorder("");
		mainPanel.setBorder(tabBorder);
		ColorMouseListener l = new ColorMouseListener();
		for(int row = 0; row < colorArray.length; row++){
			//16
			ColorComponent c = new ColorComponent(colorArray[row], width);
			c.addMouseListener(l);
			int i = row%16;
			if(i != 15){
				mainPanel.add(c, new GB(i, row/16, 1, 1)
				.setInsets(0, 2, 0, 0));
			}
			else{
				mainPanel.add(c, new GB(i, row/16, 1, 1)
				.setInsets(0, 2, 0, 0));
				mainPanel.add(new JLabel(), new GB(20, row/16, 1, 1).setFill(GB.BOTH)
						.setWeight(100, 0));
			}
		}
		mainPanel.add(new JPanel(), new GB(0, colorArray.length+1, 4, 1).setFill(GB.BOTH).setWeight(100, 100));
		return mainPanel;
	}
	public class ColorMouseListener extends MouseAdapter{
		public ColorMouseListener(){
			
		}
		public void mouseEntered(MouseEvent e) {
			Object obj = e.getSource();
	    	if(obj != null && obj instanceof ColorComponent){
	    		ColorComponent colorCom = (ColorComponent)obj;
	    		colorCom.setBorder(BorderFactory.createLineBorder(new Color(0xa0a0a0)));
	    	}
		}

	    public void mouseExited(MouseEvent e) {
	    	Object obj = e.getSource();
	    	if(obj != null && obj instanceof ColorComponent){
	    		ColorComponent colorCom = (ColorComponent)obj;
	    		colorCom.setBorder(BorderFactory.createLineBorder(colorCom.getBackground()));
	    	}
	    }
	    public void mouseClicked(MouseEvent e) {
	    	Object obj = e.getSource();
	    	if(obj != null && obj instanceof ColorComponent){
	    		ColorComponent colorCom = (ColorComponent)obj;
	    		Color color = colorCom.getColor();
	    		colorPan.setBackground(color);
		    	textPan.setForeground(color);
		    	String hex = Integer.toHexString(color.getRGB());
		    	htmlText.setText(hex.replaceFirst("ff", "#"));
		    	redText.setText(String.valueOf(color.getRed()));
		    	greenText.setText(String.valueOf(color.getGreen()));
		    	blueText.setText(String.valueOf(color.getBlue()));
	    	}
	    	
	    }
	}
	
	public class SelectMouseListener extends MouseAdapter{
		private Clipboard clipbd = getToolkit().getSystemClipboard();
		public SelectMouseListener(){
			
		}
		 public void mouseClicked(MouseEvent e) {
			 if (e.getClickCount() == 2) {
					Object obj = e.getSource();
					if (obj instanceof JTextField) {
						JTextField text = (JTextField) obj;

						String select = text.getText();
						text.setSelectionStart(0);
						text.setSelectionEnd(select.length());
						StringSelection clipString = new StringSelection(select);
						clipbd.setContents(clipString, clipString);
					}
				}
		 }
	}
}
