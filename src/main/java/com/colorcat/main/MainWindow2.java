package com.colorcat.main;

import javax.swing.*;
import java.awt.*;

public class MainWindow2 {
    public MainWindow2(){
        initMousePoint();
        initUI();
    }

    public void initUI(){
        JFrame jFrame = new JFrame("title");
        JButton button = new JButton("Test button");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(button);
        jFrame.setSize(300,300);
        jFrame.setVisible(true);
    }

    public void initMousePoint(){
        new Thread(() -> {
            while(true){

                Point point= MouseInfo.getPointerInfo().getLocation();
                System.out.println("point====x:" + point.x + ",y:" + point.y);
                try{
                    Thread.sleep(100);
                }
                catch(Exception e){

                }
            }
        }).start();

    }

    public static void main(String[] args) {
        try {
//			UIManager
//					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            UIManager
                    .setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new MainWindow2();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
