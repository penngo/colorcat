package com.colorcat.mouse;

import java.util.Arrays;
import java.util.List;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
/**
 * 屏幕取色工具
 * 
 * @author penngo(https://my.oschina.net/penngo)
 * @version 2017.08.28
 */
public class MouseHookStruct extends Structure
{
	public int x;
	public int y;
	public int mouseData;
	public int flags;
	public int time;
	public ULONG_PTR dwExtraInfo;
	@Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(new String[] { "x", "y", "mouseData",
                                            "flags", "time", "dwExtraInfo" });
    }
	
}
